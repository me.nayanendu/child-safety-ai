import React, {Component} from 'react';
import {AppRegistry,
  StyleSheet,
  View, TouchableOpacity, Text, Vibration,
  PermissionsAndroid,
  NativeModules,
  Platform,
  NativeEventEmitter,
  NativeAppEventEmitter,
  ToastAndroid,
  AsyncStorage,
  ActivityIndicator,
  Image,
  TextInput
} from 'react-native';
  import { RNCamera } from 'react-native-camera';
  import SimilarFaces from '../screen/SimilarFaces';
  import Requestor from '../lib/Requestor';
  
  import { stringToBytes } from 'convert-string';
  
  import Blemanager, { connect } from 'react-native-ble-manager';
  import Sound from 'react-native-sound';
  
  var Buffer = require('buffer/').Buffer

  const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

//const charecter = await AsyncStorage.getItem("charecter");

//const dataload = stringToBytes(charecter);

  const image_picker_options = {
    title: 'Select Photo', 
    takePhotoButtonTitle: 'Take Photo...', 
    chooseFromLibraryButtonTitle: 'Choose from Library...',
    cameraType: 'back', 
    mediaType: 'photo', 
    maxWidth: 480,
    quality: 1, 
    noData: false, 
  };
  


  let facelist_id = 'facelist_005';
  let facelist_data = {
    name: 'My 5th facelist'
  };
  
  let face_api_base_url = 'https://makerfacerecog2.cognitiveservices.azure.com/';

  //the API Key that you got from Microsoft Azure
  const api_key = 'YOUR FACE API KEY';

export default class Home extends Component {
    constructor(props){
        super(props);
        this.takePicture = this.takePicture.bind(this);
       
        this.state = {
          redIconStyle:'flex',
          greenIconStyle:'none',
          verified:0,
          totalFace:0,
          currentTemp:'',
          name: '',
              photo_style: {
                  width: 480,
                  height: 480
              },
              photo: null,
          similar_photo: null,
          message: '',
          key: "a3b0681902f84fc194f584666e87d37c",
          ble: [],
          scanningg: false,
          fullName: '',
          character: '',
          character2:'',
          setstring:'',
          rename:'',
          idofble: '',
          isButtonVisible: false,
          blename: '',
          showView: '',
          style1: 'none',
          style2: 'none',
          passcode:'',
          verifiedFaceId:[]
          };

          this.playTapAudio = this.playTapAudio.bind(this);
          this.checkPasscode = this.checkPasscode.bind(this);
          var data = new FormData();
    }
   
    playTapAudio=(text)=>{ console.log("tapping sound");
      this.setState({passcode: text});
      var tap_sound = new Sound('u_click.mp3', Sound.MAIN_BUNDLE, (error) => {
        if (error) {
          console.log('failed to load the sound', error);
          return;
        }
        // loaded successfully
        console.log('duration in seconds: ' + tap_sound.getDuration() + 'number of channels: ' + tap_sound.getNumberOfChannels());
       
        // Play the sound with an onEnd callback
        tap_sound.play((success) => {
          if (success) {
            console.log('successfully finished playing');
          } else {
            console.log('playback failed due to audio decoding errors');
          }
        });
      });
    }

    checkPasscode=async()=>{ 
      if(this.state.passcode!=''){
          var store_passcode = await AsyncStorage.getItem("passcode");
          if(store_passcode==this.state.passcode){
            //true event
            this.setState({
              
              style1:'flex',
              style2: 'none'
            });
            setTimeout(() => {this.setState({style1: 'none',style2: 'none'})}, 4000);
            const DURATION = 100;
            Vibration.vibrate(DURATION);
           console.log("before connecting");
    
            this.connectble();
            
          }else{
            var error_sound = new Sound('gnome_error.mp3', Sound.MAIN_BUNDLE, (error) => {
              if (error) {
                console.log('failed to load the sound', error);
                return;
              }
              // loaded successfully
              console.log('duration in seconds: ' + error_sound.getDuration() + 'number of channels: ' + error_sound.getNumberOfChannels());
             
              // Play the sound with an onEnd callback
              error_sound.play((success) => {
                if (success) {
                  console.log('successfully finished playing');
                } else {
                  console.log('playback failed due to audio decoding errors');
                }
              });
            });

            ToastAndroid.show("Passcode not matched",ToastAndroid.SHORT);

            this.setState({style2:'flex'});

            setTimeout(() => {this.setState({style2: 'none'})}, 4000)

          }
      }else{
        return false;
      }
    }

    getTotalFace=()=>{ console.log("calling total face");
      
    }
componentDidMount=async()=>{

  Sound.setCategory('Playback');
//connect to ble


  var total_face=0;
//get total register face
   Requestor.request(
      //face_api_base_url + '/face/v1.0/facelists/' + facelist_id+'?returnRecognitionModel=True',
      face_api_base_url + '/face/v1.0/facelists/' + facelist_id,
      'GET',
      this.state.key
      //JSON.stringify(facelist_data)
    )
    .then((res)=>{
      
      total_face = parseInt(res.persistedFaces.length); 
      this.setState({ totalFace:total_face }); 
    console.log("total reg face"+this.state.totalFace);
    });

    


       /* const blename = await AsyncStorage.getItem("ble_name");
        this.setState({blename:blename});
        const charecter = await AsyncStorage.getItem("charecter");
        this.setState({ character: charecter });*/
       
        

       await AsyncStorage.getItem("ble_name").then((value)=> {
          this.setState({ blename: value });
          Blemanager.enableBluetooth()
        .then(() => {
          // Success code
          PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
            if (result) {
               
            } else {
                PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
                    if (result) {

                       
                    } else {
                        console.log("User refuse");
                    }
                });
            }
          });
          console.log('The bluetooth is already enabled or the user confirm');
        })
        .catch((error) => {
          // Failure code
          console.log('The user refuse to enable bluetooth');
        });
          
          console.log("ssss", this.state.blename)

      }).done(); 
      await AsyncStorage.getItem("charecter").then((value) => {
          this.setState({ character: value });
          console.log("ssss", this.state.character)

      }).done();
      await AsyncStorage.getItem("charecter2").then((value) => {
        this.setState({ character2: value });
        console.log("ssss", this.state.character2)

    }).done();

    
      //this.setState({blename:"Alsaleh"});
      //this.setState({character:'A'});

        //console.log("ble name "+this.state.blename+" charecter: "+this.state.charecter);
}


toast() {
  ToastAndroid.show('Enter your Bluetooth name', ToastAndroid.SHORT);
}

toast1() {
  ToastAndroid.show('Youe bluetooth not found !', ToastAndroid.SHORT);

  var error_sound = new Sound('gnome_error.mp3', Sound.MAIN_BUNDLE, (error) => {
    if (error) {
      console.log('failed to load the sound', error);
      return;
    }
    // loaded successfully
    console.log('duration in seconds: ' + error_sound.getDuration() + 'number of channels: ' + error_sound.getNumberOfChannels());
   
    // Play the sound with an onEnd callback
    error_sound.play((success) => {
      if (success) {
        console.log('successfully finished playing');
      } else {
        console.log('playback failed due to audio decoding errors');
      }
    });
  });
  error_sound.setVolume(1);
}

handleDiscoverPeripheral=async(data)=>{
  const { ble } = this.state
  this.setState({ ble: [...ble, data] }, () => {
       //console.log('Got ble data ', ble);
       //ToastAndroid.show("Got ble data ",ble,ToastAndroid.LONG);
      //console.log('local name');
      //console.log(data.advertising.localName);
  })
}

diconnect() {
  Blemanager.disconnect(this.state.idofble)
      .then(() => {
          console.log('Disconnected');
          ToastAndroid.show('Disconnected!!', ToastAndroid.SHORT);

      })
      .catch((error) => {
          console.log(error);
      });
}

connectble=async()=> { console.log("BLE "+this.state.blename);


  if (this.state.blename != "") {
      Blemanager.start({ showAlert: false });
      this.handleDiscoverPeripheral = await this.handleDiscoverPeripheral.bind(this);

      NativeAppEventEmitter
          .addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral);

      await Blemanager.scan([], 5, true)
          .then((results) => { 
            ToastAndroid.show("Searching for "+this.state.blename,ToastAndroid.LONG);
            console.log('Scanning...'); console.log(results); });

            console.log("My Bluetooth is"+this.state.blename);

      if (this.state.ble.find(obj => obj.advertising.localName === this.state.blename)) {


          const bluetoothInstance = this.state.ble.find(obj => obj.advertising.localName === this.state.blename)
          this.setState({ idofble: bluetoothInstance.id })

          

          await Blemanager.connect(bluetoothInstance.id)
              .then(() => {

                ToastAndroid.show("DEVICE CONNECTED",ToastAndroid.SHORT);
                  console.log('Connected!!');
                  console.log(this.state.idofble);
                  ToastAndroid.show('Connected!!', ToastAndroid.SHORT);
                  Blemanager.retrieveServices(this.state.idofble)
                      .then((peripheralInfo) => {

                        //alert("Starting receving data from BLE every 500 mls");
                        // this.reqTempInterval = setInterval(()=>{
                          
                        // },500)
                          console.log('Peripheral info:', peripheralInfo);
                      });

              })
              .catch((error) => {

                  console.log("error:", error);
              });

      }
      else {
          this.toast1();
          
      }
  }

  else {

      this.toast();
  }
}

sendchar=()=>{ 
  var sendgift='';
  var sendd;
  if(this.state.verified==this.state.totalFace){ 
    sendgift = this.state.character2;
  }else{ 
    sendgift = this.state.character;
  } 
  if(sendgift==null){
    ToastAndroid.show("Please Setup Charecter",ToastAndroid.SHORT);
    return false;
  }
    sendd = stringToBytes(sendgift)
  console.log('>>>>>>>>>>before write');
  Blemanager.write(this.state.idofble, 'FFE0', 'FFE1', sendd)

      .then(() => {
          console.log('.....Write: ' + sendd);
        
       // this.setState({currentTemp:returnData});

          var success_sound = new Sound('success_ding.mp3', Sound.MAIN_BUNDLE, (error) => {
            if (error) {
              console.log('failed to load the sound', error);
              return;
            }
            // loaded successfully
            console.log('duration in seconds: ' + success_sound.getDuration() + 'number of channels: ' + success_sound.getNumberOfChannels());
           
            // Play the sound with an onEnd callback
            success_sound.play((success) => {
              if (success) {
                console.log('successfully finished playing');
              } else {
                console.log('playback failed due to audio decoding errors');
              }
            });
          });

          ToastAndroid.show('Data Sent!!', ToastAndroid.SHORT);

          this.setState({ isButtonVisible: true });
          
          Blemanager.read(
            this.state.idofble, 'FFE0', 'FFE1'
          )
            .then((readData) => {
              // Success code
              console.log("Read: " + readData);
             // alert(readData);
              const buffer = Buffer.Buffer.from(readData); //https://github.com/feross/buffer#convert-arraybuffer-to-buffer
              var sensorData = buffer.readUInt8(1, true);
              alert(sensorData);
              this.setState({currentTemp: readData.toString()});
            })
            .catch((error) => {
              // Failure code
              console.log(error);
            });
      })
      .catch((error) => {
          console.log(error);
          ToastAndroid.show(error,ToastAndroid.SHORT);
      });
}

takePicture = async() => {
 
  const DURATION = 100;
        Vibration.vibrate(DURATION);

  Blemanager.enableBluetooth()
  .then(() => {
    // Success code
    PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
      if (result) {
          console.log("Permission is OK");
      } else {
          PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
              if (result) {
                  console.log("User accept");
              } else {
                  console.log("User refuse");
              }
          });
      }
    });
    console.log('The bluetooth is already enabled or the user confirm');
  })
  .catch((error) => {
    // Failure code
    console.log('The user refuse to enable bluetooth');
  });
  

  if (this.camera) {
    const options = { quality: 0.5, base64: true };
    const response = await this.camera.takePictureAsync(options);
    
    ToastAndroid.show("Matching your face...",ToastAndroid.SHORT);

    console.log(response);
    
    await AsyncStorage.setItem('image',response.base64);

    //data.append("image",response.base64);
    /*fetch('https://questanya.com/faceapp2/index.php',{
           method: "POST",
           header: {
            Accept: 'application/json',
            'Content-Type': 'application/json',        
           },
           body: 
            data
           
        }
      ).then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
      })
      .catch((error) => {
        console.error(error);
      }); */
    console.log("width:"+response.width);
    console.log("height:"+response.height);
    console.log("uri:"+response.uri);

    let source = {uri: response.uri};
          
          this.setState({
            photo_style: {
              width: response.width,
              height: response.height
            },
            photo: source,
            photo_data: response.base64
          });

          this._getSimilarFace();
  }
};

async _getSimilarFace() {
    var form_data = new FormData();
    var faceImg = await AsyncStorage.getItem("image");
    form_data.append("image",faceImg);
  Requestor.upload(
    face_api_base_url + '/face/v1.0/detect',
    this.state.key,
    this.state.photo_data
  )
  .then((facedetect_res) => { 
    if(facedetect_res.length==0){

      this.setState({style2:'flex'});

      setTimeout(() => {this.setState({style2: 'none'})}, 4000)

      var error_sound = new Sound('gnome_error.mp3', Sound.MAIN_BUNDLE, (error) => {
        if (error) {
          console.log('failed to load the sound', error);
          return;
        }
        // loaded successfully
        console.log('duration in seconds: ' + error_sound.getDuration() + 'number of channels: ' + error_sound.getNumberOfChannels());
       
        // Play the sound with an onEnd callback
        error_sound.play((success) => {
          if (success) {
            console.log('successfully finished playing');
          } else {
            console.log('playback failed due to audio decoding errors');
          }
        });
      });

      
      form_data.append("is_allowed",'0');
          
          fetch('https://questanya.com/faceapp2/index.php',{
                  method: "POST",
                  header: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',        
                  },
                  body: 
                  form_data
                  
              }
            ).then((response) => {console.log(response); response.json()})
            .then((responseJson) => {
              console.log(responseJson);
            })
            .catch((error) => {
              console.error(error);
            });



      return false;
    }
    let face_id = facedetect_res[0].faceId;
    

    let data = {
      faceId: face_id,
      faceListId: facelist_id,
      maxNumOfCandidatesReturned: 2
    }

    Requestor.request(
      face_api_base_url + '/face/v1.0/findsimilars', 
      'POST', 
      this.state.key,
      JSON.stringify(data)
    )
    .then((similarfaces_res) => {
      console.log("similar face");
      console.log(similarfaces_res);

      if(similarfaces_res.length==0){

        this.setState({style2:'flex'});

        setTimeout(() => {this.setState({style2: 'none'})}, 4000)

        var error_sound = new Sound('gnome_error.mp3', Sound.MAIN_BUNDLE, (error) => {
          if (error) {
            console.log('failed to load the sound', error);
            return;
          }

          
        
          form_data.append("is_allowed",'0');
          console.log(form_data);
           fetch('https://questanya.com/faceapp2/index.php',{
                  method: "POST",
                  header: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',        
                  },
                  body: form_data
                  
              }
            ).then((response) => {console.log(response); response.json()})
            .then((responseJson) => {
              console.log(responseJson);
            })
            .catch((error) => {
              console.error(error);
            });

          // loaded successfully
          console.log('duration in seconds: ' + error_sound.getDuration() + 'number of channels: ' + error_sound.getNumberOfChannels());
        
          // Play the sound with an onEnd callback
          error_sound.play((success) => {
            if (success) {
              console.log('successfully finished playing');
            } else {
              console.log('playback failed due to audio decoding errors');
            }
          });
        });
        
        return false;
      }

      let similar_face = similarfaces_res[0];
      
      Requestor.request(
        face_api_base_url + '/face/v1.0/facelists/' + facelist_id,
        'GET',
        this.state.key
      )
      .then((facelist_res) => {

        let user_data = {}; 
        facelist_res['persistedFaces'].forEach((face) => {
          if(face.persistedFaceId == similar_face.persistedFaceId){
            user_data = JSON.parse(face.userData);
          }
        }); 

        var verifiedFaceId = this.state.verifiedFaceId; console.log("list verified array ",verifiedFaceId); console.log("length face"+verifiedFaceId.indexOf(similar_face.persistedFaceId));
        if(verifiedFaceId.indexOf(similar_face.persistedFaceId)==-1){
            this.setState({ verifiedFaceId: [...this.state.verifiedFaceId, similar_face.persistedFaceId] });
        //}
        
        
            //onsuccess
            this.setState({
              similar_photo: {uri: user_data.filename},
              message: 'Similar to: ' + user_data.name + ' with confidence of ' + similar_face.confidence,
              style1:'flex',
              style2: 'none',
              verified: (this.state.verified+1)
            });
            setTimeout(() => {this.setState({style1: 'none',style2: 'none'})}, 4000);
            const DURATION = 100;
            Vibration.vibrate(DURATION);
          console.log("before connecting");

          form_data.append("is_allowed",'1');
          
          fetch('https://questanya.com/faceapp2/index.php',{
                  method: "POST",
                  header: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',        
                  },
                  body: 
                  form_data
                  
              }
            ).then((response) => response.json())
            .then((responseJson) => {
              console.log("server response",responseJson);
            })
            .catch((error) => {
              console.error("server error ",error);
            });

            ToastAndroid.show("Face Matched",ToastAndroid.SHORT);
           

            if(this.state.verified==this.state.totalFace){
              this.setState({redIconStyle:'none',greenIconStyle:'flex'});
              
            }
            
              //this.sendchar();
            
       

          }else{
            ToastAndroid.show("Face already scanned");
          }
      });
      
    });
    
  });

}


    render() {
        return (
          
         /* <View style={styles.container}>
           <View style={{ height: 100, backgroundColor: "white",}}>
             <TouchableOpacity onPress={()=>{this.props.navigation.navigate("Setting")}}>
            <Text style={{fontSize: 15, fontWeight: "bold", textAlign: "right"}}>Settings</Text>
            </TouchableOpacity>
          </View>
            
            <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.front}
          flashMode={RNCamera.Constants.FlashMode.on}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          onGoogleVisionBarcodesDetected={({ barcodes }) => {
            console.log(barcodes);
          }}
        />
       
                 
                
    <Text style={{color:"#fff"}}>{this.state.message}</Text>
        <View style={[{ flex: 0, flexDirection: 'row', justifyContent: 'center' },styles.tnBx5]}>
                  <TouchableOpacity onPress={this.takePicture} style={styles.btnX1}>
                    <Text style={[{ fontSize: 14 },styles.btnX1t]}> SNAP </Text>
                  </TouchableOpacity>

                 
                </View>

                
          </View>*/

          <View style={styles.campg}>

          

<View style={styles.txtLart}>
  <View style={styles.txtLart2}>

 
              <View style={styles.txtLart3}>

            
                {/* <View style={[styles.txtaTxt, {display: this.state.style1}]}>
                  <Text style={styles.txtaTxt2}>Welcome</Text>
                </View>
                <View style={[styles.txtaImg, {display: this.state.style1}]}>
                  <Image style={{width:160, height:76}} source={require('../images/key.png')} />
                </View>
            
           
                <View style={[styles.txtaTxt, {display: this.state.style2}]}>
                  <Text style={styles.txtaTxt3}>Not Authorized</Text>
                </View>
                <View style={[styles.txtaImg, {display: this.state.style2}]}>
                  <Image style={styles.blkimg} source={require('../images/block.png')} />
                </View> */}
            
            <View style={styles.txtaImg}>
              <Image style={[{width:30, height:30},{display:this.state.redIconStyle}]} source={require('../images/red_i.png')} />
              <Image style={[{width:30, height:30},{display:this.state.greenIconStyle}]} source={require('../images/green_i.png')} />
            </View>
            <View style={styles.txtaTxt}>
              <Text style={styles.txtaTxt2}>{this.state.verified}/{this.state.totalFace}</Text>
            </View>
            
              </View>

  </View>
</View>




<View style={styles.rbtn}>
  <View style={styles.rbtn2}>

  {/* <TouchableOpacity style={styles.rbtn3} activeOpacity={0.8} onPress={this.takePicture}>
    <Text style={styles.rbtnT1}>Open{'\n'}Door</Text>
  </TouchableOpacity> */}


  <TouchableOpacity style={[styles.rbtn3,{backgroundColor:'#3DA5D9'}]} activeOpacity={0.8} onPress={this.takePicture}>
      <Text style={styles.rbtnT1}>Verify Face</Text>
    </TouchableOpacity>

    <View style={styles.twoBtn}>
        <TouchableOpacity style={[styles.rbtn4,{backgroundColor:'#FE4A49'}]} activeOpacity={0.8} onPress={this.connectble}>
          <Text style={styles.rbtnT1}>Connect</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.rbtn4,{backgroundColor:'#1B9AAA'}]} activeOpacity={0.8} onPress={this.sendchar}>
          <Text style={styles.rbtnT1}>Send Char</Text>
        </TouchableOpacity>
    </View>
  </View>
</View>



<RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.front}
          flashMode={RNCamera.Constants.FlashMode.on}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          onGoogleVisionBarcodesDetected={({ barcodes }) => {
            console.log(barcodes);
          }}
        />



{/* <View style={styles.cam1}>
<View style={styles.cam2}>

    <View style={styles.cam3}>
      
      <Image style={{width:250, height:250}} source={require('../images/cam.png')} />
      
    </View>


</View>
</View> */}

<TouchableOpacity style={styles.seting} activeOpacity={0.8} onPress={()=>{this.props.navigation.navigate("Setting")}}>
            <Image style={{width:20, height:20}} source={require('../images/gear.png')} />
          </TouchableOpacity>

<View style={styles.codBx1}>
  <View style={styles.codBx2}>
{/* 
  <TextInput
  style={styles.codin}
  placeholder={'Code'}
  placeholderTextColor={'#9A8BC2'}
  onChangeText={(text)=>{ this.playTapAudio(text)}}
  keyboardType={'number-pad'}
  secureTextEntry={true}
  />

  <TouchableOpacity style={styles.gobtn} activeOpacity={0.8} onPress={this.checkPasscode}>
    <Image style={{width:20, height:20}} source={require('../images/go.png')} />
  </TouchableOpacity> */}


<Text style={styles.codin}>Return: {this.state.currentTemp}</Text>

</View>
</View>

</View>   


        )
    }
  }
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    
    backgroundColor: 'black',
  },
  loader:{
    flex:1
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  tnBx5:{
    paddingTop:10,
  },
  btnX1:{
    backgroundColor:'#4a3ad0',
    paddingTop:11,
    paddingBottom:13,
    borderRadius:2,
    width: "100%"
  },
  btnX1t:{ textAlign:'center',
    color:'#ffffff', 
  },
  campg:{width:'100%', flex:1, flexDirection:'column', justifyContent:'space-between', height:'100%', backgroundColor:'#000',},
  txtLart:{ position:'absolute', width:'100%', top:40, zIndex:9, },
  txtLart2:{flex:1, flexDirection:'column', justifyContent:'center', width:'100%', alignItems:'center',},
  txtLart3:{backgroundColor:'#fff', paddingTop:10, paddingBottom:10, paddingLeft:15, paddingRight:15, borderRadius:4, flex:1, flexDirection:'row',},
  txtaTxt:{paddingBottom:5,},
  txtaTxt2:{color:'#03041A', textAlign:'center', fontSize:22,},
  txtaTxt3:{color:'#D55454', textAlign:'center', fontSize:22,},
  txtaImg:{},
  blkimg:{ width:70, height:70, left:'50%', marginLeft:-35,},

  rbtn:{position:'absolute', width:'100%', bottom:70, zIndex:9,},
  rbtn2:{flex:1, flexDirection:'column', justifyContent:'center', width:'100%', alignItems:'center',},
  
  twoBtn:{width:'90%', flex:1, flexDirection:'row',justifyContent:'center'},
  rbtn3:{backgroundColor:'#06C24A', width:'90%', borderRadius:6, paddingTop:12, paddingBottom:12,  flex:1, flexDirection:'column', justifyContent:'center',},
  rbtn4:{backgroundColor:'#06C24A', width:'40%', borderRadius:6, paddingTop:12, marginTop:5,marginRight:5, paddingBottom:12,  justifyContent:'center'},
  rbtnT1:{justifyContent: 'center', color:"#fff", fontSize:22, fontWeight:'bold', textAlign: 'center',},


inpset:{
      marginBottom:15,
    },
    
    // seting:{padding:12,position:'absolute', right:0, top:0,},
    // codBx1:{paddingTop:5, paddingBottom:5, paddingLeft:10, paddingRight:10,position:'absolute', bottom:10, left:0,backgroundColor:'#fff',width:'100%', },
    // codBx2:{},
    // codin:{height:40,},
    // gobtn:{backgroundColor:'#06C24A', position:'absolute', right:0, top:0, height:40,width:40,padding:10, borderRadius:20,},
  
    seting:{padding:12,position:'absolute', right:0, top:0,},
  codBx1:{paddingTop:15, paddingBottom:15, paddingLeft:10, paddingRight:10,position:'absolute', bottom:0, left:0,backgroundColor:'#fff',width:'100%', },
  codBx2:{},
  codin:{textAlign: 'center'},
  gobtn:{backgroundColor:'#06C24A', position:'absolute', right:0, top:0, height:40,width:40,padding:10, borderRadius:20,},

  cam1:{flex:1, flexDirection:'row', justifyContent:'center', width:'100%', alignItems:'center',},
  cam2:{},
  cam3:{width:'100%', textAlign: 'center', opacity: .2,},
});


