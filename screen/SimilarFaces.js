import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    Button,
    AsyncStorage,
    TouchableOpacity,
    FlatList,
    ToastAndroid,
    Alert,
    PermissionsAndroid,
    RefreshControl
  } from 'react-native';
  
  import React, { Component } from 'react';
  
  import ImagePickerManager  from 'react-native-image-picker';
  //import Button from 'react-native-button';
  
  import Requestor from '../lib/Requestor';
  
  let facelist_id = 'facelist_005';
  let facelist_data = {
    name: 'My 5th facelist'
  };
  
  let face_api_base_url = 'https://makerfacerecog2.cognitiveservices.azure.com/';
  const image_picker_options = {
    title: 'Select Photo',
    takePhotoButtonTitle: 'Take Photo...',
    chooseFromLibraryButtonTitle: 'Choose from Library...',
    cameraType: 'back',
    mediaType: 'photo',
    maxWidth: 480,
    quality: 1,
    noData: false,
  };

  function wait(timeout) {
    return new Promise(resolve => {
      setTimeout(resolve, timeout);
    });
  }

  function Item({userData,faceId,object}){
    var index = 1;
    var inner_data = JSON.parse(userData);
    return (
      <View style={styles.lstBx} key={faceId}>
              <View style={styles.lstClr}>

               

                <View style={styles.lSet2}> 
                  
                    <View style={styles.lpic}>
                      <Image style={{width:60, height:60, borderRadius:16}} source={{uri: inner_data.filename}} />
                    </View>

                    <View style={[styles.lname,{paddingTop: 20, fontWeight: 'bold'}]}>
                      <Text style={styles.ltxt3}>{inner_data.name}</Text>
                    </View>
                   
                  <View style={styles.lbtn}>
                    <TouchableOpacity style={styles.dltBtn} activeOpacity={0.8} onPress={()=>{object.removeFace(faceId,object)}} >
                      <Image style={{width:18, height:18}} source={require('../images/bin.png')} />
                    </TouchableOpacity>
                  </View>
                </View>

              </View>
              </View>
    );
  }
  
  export default class SimilarFaces extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          name: '',
          photo_style: {
              width: 480,
              height: 480
          },
          photo: null,
          similar_photo: null,
          message: '',
          key: "a3b0681902f84fc194f584666e87d37c",
          hardwareName:'',
          charecter:'',
          allface:[],
          uniqueValue: 1,
          refresh: 1,
          passcode:'',
          charecter2:''
        };
        
        this._getFaceList = this._getFaceList.bind(this); 
        this.removeFace = this.removeFace.bind(this);
        //this.forceUpdateHandler = this.forceUpdateHandler.bind(this);
        
        
    }

    

      
   /* forceUpdateHandler(){
      this.forceUpdate();
    };*/
   
    removeFace=(faceId,obj)=>{ console.log(this.state.key);
      obj = this;
      Alert.alert(
        'Attention!',
        'Are you sure to remove this face?',
        [
          {text: 'Delete',onPress:()=>{
            console.log(faceId);
            
            Requestor.request(
              face_api_base_url + '/face/v1.0/facelists/'+facelist_id+'/persistedFaces/'+faceId,
              'DELETE',
              this.state.key
              //JSON.stringify(facelist_data)
            )
            .then(function(res){
              ToastAndroid.show("Face Removed",ToastAndroid.SHORT);
              //obj.forceUpdateHandler;
              //window.location.reload(false);
              obj.setState({refresh: 2});
            })
      
            //face/v1.0/facelists/{faceListId}/persistedFaces/{persistedFaceId
          }},
        {text: 'No', onPress:()=>{
          style: 'cancel'
        }}
        ]
      )
    }
    componentDidMount=async()=>{
      var name = await AsyncStorage.getItem("ble_name");
      var charecter = await AsyncStorage.getItem("charecter");
      var passcode = await AsyncStorage.getItem("passcode");
      var charecter2 = await AsyncStorage.getItem("charecter2");
      let currentComponent = this;      
      console.log(name);
      console.log(charecter);
      this.setState({hardwareName:name});
       this.setState({charecter:charecter});
       this.setState({passcode:passcode});
       this.setState({charecter2:charecter2});
       this._getFaceList(this);

       PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE).then((result) => {
        if (result) {
            console.log("Permission is OK");
        } else {
            PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE).then((result) => {
                if (result) {
                    console.log("User accept");
                } else {
                    console.log("User refuse");
                }
            });
        }
      });
      PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE).then((result) => {
        if (result) {
            console.log("Permission is OK");
        } else {
            PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE).then((result) => {
                if (result) {
                    console.log("User accept");
                } else {
                    console.log("User refuse");
                }
            });
        }
      });
      PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
        if (result) {
            console.log("Permission is OK");
        } else {
            PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
                if (result) {
                    console.log("User accept");
                } else {
                    console.log("User refuse");
                }
            });
        }
      });
    }
    saveBleData=async()=>{ console.log("saving info");
      await AsyncStorage.setItem('ble_name', this.state.hardwareName);
      await AsyncStorage.setItem('charecter',this.state.charecter);
     // await AsyncStorage.setItem('passcode',this.state.passcode);
      await AsyncStorage.setItem("charecter2",this.state.charecter2);
      ToastAndroid.show('Information stored', ToastAndroid.SHORT);
    }
    
    
    render() {
      
      
      return (
       
      <View style={styles.fulpg}>
<ScrollView style={styles.whtbg}>

<View style={styles.picbox2}>
<View style={styles.picbox}>
  <Image style={{width:250, height:250}} source={this.state.photo}
              resizeMode={"contain"} />
</View>
</View>


<View style={styles.btnPnl}>
  <View style={styles.btn2}>
    <TouchableOpacity style={styles.btnX2} activeOpacity={0.8} onPress={()=>{this.setState({photo:null})}}>
      <Text style={styles.btnX2t}>Remove</Text>
    </TouchableOpacity>
  </View>
  <View style={styles.btn3}>
    <TouchableOpacity style={styles.btnX3} activeOpacity={0.8} onPress={this._pickImage.bind(this)}>
      <Text style={styles.btnX3t}>Take Pic</Text>
    </TouchableOpacity>
  </View>
  
  

    


</View>


<View style={styles.frmBx}>
      
      <View style={styles.inpset}>
        <Text style={styles.label}>
          Face Name {this.state.refresh}
        </Text>
        <TextInput
            style={styles.input1}
            placeholder={'Face Name'}
            placeholderTextColor={'#9c9c9c'}
            value={this.state.name}
            onChangeText={this._changeName.bind(this)}
          />
      </View>
      </View>

<View style={styles.frmBx}>
<View style={styles.tnBx5}>
    <TouchableOpacity style={styles.btnX1} activeOpacity={0.8} onPress={this._addFaceToFaceList.bind(this)}>
      <Text style={styles.btnX1t}>Register Face</Text>
    </TouchableOpacity>
  </View>
  </View>

<View style={styles.frmBx}>
  
  <View style={styles.inpset}>
    <Text style={styles.label}>
      Hardware Name 
    </Text>
    <TextInput
        style={styles.input1}
        placeholder={'Hardware Name'}
        placeholderTextColor={'#9c9c9c'}
        value={this.state.hardwareName}
        onChangeText={(text) => this.setState({hardwareName: text})}
      />
  </View>

  <View style={styles.inpset}>
    <Text style={styles.label}>
    Character
    </Text>
    <TextInput
        style={styles.input1}
        placeholder={'Character'}
        placeholderTextColor={'#9c9c9c'}
        value={this.state.charecter}
        onChangeText={(text) => this.setState({charecter: text})}
      />
  </View>

  <View style={styles.inpset}>
    <Text style={styles.label}>
      Character 2
    </Text>
    <TextInput
        style={styles.input1}
        placeholder={'Character 2'}
        placeholderTextColor={'#9c9c9c'}
        value={this.state.charecter2}
        onChangeText={(text) => this.setState({charecter2: text})}
       
      />
  </View>

  <View style={styles.tnBx5}>
    <TouchableOpacity style={styles.btnX1} onPress={this.saveBleData.bind(this)} activeOpacity={0.8}>
      <Text style={styles.btnX1t}>SAVE</Text>
    </TouchableOpacity>
  </View>

</View>



<View style={styles.lstbg}>
<ScrollView>
<FlatList
        data={this.state.allface}
        renderItem={({ item }) => <Item userData={item.userData} faceId={item.persistedFaceId} object={this}  />}
        keyExtractor={item => item.persistedFaceId}
        extraData={this.state}
        showsVerticalScrollIndicator={true}
/>
</ScrollView>


</View>

</ScrollView>

</View> 
      );
    }
  
  
    _changeName(text) {
      this.setState({
        name: text
      });
    }
  
    _pickImage() {
  
      ImagePickerManager.showImagePicker(image_picker_options, (response) => { console.log(response);
           
        if(response.error){
          alert('Error getting the image. Please try again.');
        }else{
          console.log("data:"+response.data);
          let source = {uri: response.uri};
          
          this.setState({
            photo_style: {
              width: response.width,
              height: response.height
            },
            photo: source,
            photo_data: response.data
          });
           
        }
      });
     
    }
  
    _createFaceList=()=>{
  
      Requestor.request(
        face_api_base_url + '/face/v1.0/facelists/' + facelist_id,
        'PUT',
        this.state.key,
        JSON.stringify(facelist_data)
      )
      .then(function(res){
        alert('Face List Created!');
      });
  
    }
  
    _getFaceList=(e)=> {
  
      Requestor.request(
        //face_api_base_url + '/face/v1.0/facelists/' + facelist_id+'?returnRecognitionModel=True',
        face_api_base_url + '/face/v1.0/facelists/' + facelist_id,
        'GET',
        this.state.key
        //JSON.stringify(facelist_data)
      )
      .then(function(res){
        //alert('Face List available!');
        console.log(res.persistedFaces);
        e.setState({allface:res.persistedFaces});

        
         //var list_face = [];
         //var faceId='';
        /*for(var i = 0;i<res.persistedFaces.length;i++){
          faceId = res.persistedFaces[i].persistedFaceId;
          
          const get_inner  =  JSON.parse(res.persistedFaces[i].userData); 
          var index = i;
            index = parseInt(index + 1);
           list_face.push(
            <View style={styles.lstBx} key={faceId}>
              <View style={styles.lstClr}>

                <View style={styles.lSet1}> 
           <Text style={styles.ltxt1}>{index} {faceId}</Text>
                </View>

                <View style={styles.lSet2}> 
                  
                    <View style={styles.lpic}>
                      <Image style={{width:60, height:60, borderRadius:16}} source={{uri: get_inner.filename}} />
                    </View>

                    <View style={styles.lname}>
                      <Text style={styles.ltxt3}>{get_inner.name}</Text>
                    </View>
                   
                  <View style={styles.lbtn}>
                    <TouchableOpacity style={styles.dltBtn} activeOpacity={0.8} onPress={()=>{e.removeFace(res.persistedFaces[i])}}>
                      <Image style={{width:18, height:18}} source={require('../images/bin.png')} />
                    </TouchableOpacity>
                  </View>
                </View>

              </View>
              </View>
          )
        }*/
        //const faces = JSON.parse(res); console.log(faces);
        //this.setState({allface:JSON.parse(res.persistedFaces)});
        //console.log(list_face);
        //e.setState({allface:list_face});
      });
  
    }

    _addFaceToFaceList() {
  
      var user_data = {
        name: this.state.name,
        filename: this.state.photo.uri
      };
  
      Requestor.upload(
        face_api_base_url + '/face/v1.0/facelists/' + facelist_id + '/persistedFaces',
        this.state.key,
        this.state.photo_data,
        {
          userData: JSON.stringify(user_data)
        }
      )
      .then((res) => { console.log(res);
        if(typeof res.error != 'undefined'){
          alert(res.error.message);
          this._createFaceList();
        }else{
          alert('Face was added to face list!');
        }
      
      });
     
    }
  
    _getSimilarFace() {
    
      Requestor.upload(
        face_api_base_url + '/face/v1.0/detect',
        this.state.key,
        this.state.photo_data
      )
      .then((facedetect_res) => {
        
        let face_id = facedetect_res[0].faceId;
  
        let data = {
          faceId: face_id,
          faceListId: facelist_id,
          maxNumOfCandidatesReturned: 2
        }
  
        Requestor.request(
          face_api_base_url + '/face/v1.0/findsimilars', 
          'POST', 
          this.state.key,
          JSON.stringify(data)
        )
        .then((similarfaces_res) => {
  
          let similar_face = similarfaces_res[1];
          
          Requestor.request(
            face_api_base_url + '/face/v1.0/facelists/' + facelist_id,
            
            this.state.key
          )
          .then((facelist_res) => {
  
            let user_data = {};
            facelist_res['persistedFaces'].forEach((face) => {
              if(face.persistedFaceId == similar_face.persistedFaceId){
                user_data = JSON.parse(face.userData);
              }
            });
  
            this.setState({
              similar_photo: {uri: user_data.filename},
              message: 'Similar to: ' + user_data.name + ' with confidence of ' + similar_face.confidence
            });
  
          });
          
        });
        
      });
  
    }  
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
         alignItems: 'center'
    },
    button: {
      padding: 10,
      margin: 20,
      height: 45,
      overflow: 'hidden', 
      backgroundColor: 'white'
    },
    text_input: {
      height: 40, 
      borderColor: 'gray', 
      borderWidth: 1,
      backgroundColor: '#FFF'
    },
    message: {
      fontSize: 20,
      fontWeight: 'bold'
    },
    fulpg:{
      width:'100%',
      flex:1,
      flexDirection:'column',
      justifyContent: 'space-between',
      height:'100%',
    },
    whtbg:{
      backgroundColor:'#ffffff', 
    },
    picbox2: {
      width:'100%',
      flex:1,
      flexDirection:'column',paddingTop:20, paddingBottom:20,
    },
    picbox: {
      alignSelf: 'center',
      width:254,
      height:254,
      borderColor:'#d8d8d8',
      borderStyle:'solid',
      borderWidth:2,
    },
    inpset:{
      marginBottom:15,
    },
    label:{
      color:'#000',
      fontSize:14,
      marginBottom:10,
    },
    input1:{
      backgroundColor:'#ffffff',
      borderColor:'#c2c2c2',
      borderStyle:'solid',
      borderWidth:1,
      height:42,
      borderRadius:2,
      paddingLeft:12,
      paddingRight:12,
      color:'#000000',
    },
    frmBx:{
      paddingLeft:20,
      paddingRight:20,
      paddingBottom:20,
    },
    tnBx5:{
      paddingTop:10,
    },
    btnX1:{
      backgroundColor:'#4a3ad0',
      paddingTop:11,
      paddingBottom:13,
      borderRadius:2,
    },
    btnX1t:{ textAlign:'center',
      color:'#ffffff', 
    },
  
    btnPnl:{flex:1, flexDirection:'row', paddingLeft:20, paddingBottom:20, paddingRight:20,},
    
    btn2:{paddingRight:7, width:'50%',},
    btnX2:{backgroundColor:'#c6cbd0', paddingTop:11, paddingBottom:13, borderRadius:2,},
    btnX2t:{textAlign:'center', color:'#202325',},
    
    btn3:{paddingLeft:7, width:'50%',},
    btnX3:{backgroundColor:'#4a3ad0', paddingTop:11, paddingBottom:13, borderRadius:2,},
    btnX3t:{textAlign:'center', color:'#ffffff',},


    lstbg:{backgroundColor:'#EEEEF3',paddingLeft:20, paddingRight:20,paddingTop:20,paddingBottom:5,},
    lstBx:{marginBottom:15,},
    lstClr:{backgroundColor:'#fff',borderRadius:2,},
  
    lSet1:{padding:10, borderBottomWidth:1, borderStyle:'solid', borderColor:'#E4E4E4',},
    ltxt1:{color:'#000', fontSize:14,},
  
    lSet2:{padding:10, flex:1, flexDirection:"row", },
  
    lpic:{width:60, height:60, borderRadius:16, backgroundColor:'#EEEEF3', },
    lname:{flex:1, paddingLeft:10, paddingRight:10,},
      ltxt3:{fontSize:14, color:'#000', fontWeight:'600',},
    lbtn:{ justifyContent:'center',},
      dltBtn:{backgroundColor:'#F55858',width:38, height:38,padding:10,borderRadius:19,}, 
  
  });
  