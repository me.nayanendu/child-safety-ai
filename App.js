/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import HomeScreen from './screen/home';
import Setting from './screen/SimilarFaces';
const MainNavigator = createStackNavigator({
  Home: {screen: HomeScreen, navigationOptions: {
    header: null
  }},
  Setting: {screen: Setting, navigationOptions:{
    header: null
  }},
});

const App = createAppContainer(MainNavigator);

export default App;
