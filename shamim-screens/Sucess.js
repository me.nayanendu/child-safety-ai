
import React, { Component } from 'react';
import {
    StyleSheet,
    Button,
    View,
    TextInput,
    Image,
    TouchableHighlight,
    TouchableOpacity,
    Alert,
    NativeAppEventEmitter,
    Platform,
    PermissionsAndroid, ToastAndroid, StatusBar, ImageBackground
} from 'react-native';
import { Container, Header, Left, Body, Right, Title, Text } from 'native-base';
import Blemanager, { connect } from 'react-native-ble-manager';
import { stringToBytes } from 'convert-string';
const dataload = stringToBytes("ABC");

export default class Sucess extends Component {




    render() {

        return (
            <Container>
                <StatusBar barStyle="dark-content" hidden={true} backgroundColor="#00BCD4" translucent={true} />

                <Header style={{ backgroundColor: '#141e26' }}>

                    <Body>
                        <Image style={{ width: 150, height: 50, resizeMode: 'center', alignSelf: 'center' }} source={require('../images/Ebot-logo.png')} />

                    </Body>

                </Header>

                <View style={styles.container}>

                    <Text style={{ color: 'white', fontSize: 23, fontWeight: 'normal', marginTop: 2, alignSelf: 'center', color: 'white' }}>E-bot Gift Sucessfully Sent</Text>

                    <View>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Blescreen')}>
                            <Image style={{ width: 100, height: 100, resizeMode: 'center', alignSelf: 'center' }} source={require('../images/ebotlogo.png')} />
                            <Text style={{ color: 'white', fontSize: 13, fontWeight: 'normal', marginTop: 2, alignSelf: 'center', color: 'white', fontWeight: 'bold' }}>EXIT</Text>

                        </TouchableOpacity>

                    </View>
                </View>

            </Container>


        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#22303a',
        //opacity: 0.8,

    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#FFFFFF',
        borderRadius: 30,
        borderBottomWidth: 1,
        width: 250,
        height: 45,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center',

    },
    inputs: {
        height: 45,
        marginLeft: 16,
        borderBottomColor: '#FFFFFF',
        flex: 1,

    },
    inputIcon: {
        width: 30,
        height: 30,
        marginLeft: 15,
        justifyContent: 'center'
    },
    buttonContainer: {
        height: 80,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: 80,
        borderRadius: 60,
    },
    signupButton: {
        backgroundColor: "#0263f4",
    },
    signUpText: {
        color: 'white',
    }

});