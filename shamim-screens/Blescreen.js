import React, { Component } from 'react';
import {
    StyleSheet,
    Button,
    View,
    TextInput,
    Image,
    TouchableHighlight,
    TouchableOpacity,
    Alert,
    NativeAppEventEmitter,
    Platform,
    PermissionsAndroid, ToastAndroid, StatusBar, ImageBackground, AsyncStorage
} from 'react-native';
import { Container, Header, Left, Body, Right, Title, Text } from 'native-base';
import Blemanager, { connect } from 'react-native-ble-manager';
import { stringToBytes } from 'convert-string';
const dataload = stringToBytes("ABC");
const gift = ("$EGDG");  
const str = ("$EGSC"); 
const renameble = ("$EGBR"); 
import LottieView from 'lottie-react-native';
export default class Blescreen extends Component {

    
    constructor(props) {
        super(props)

        this.state = {
            ble: [],
            scanningg: false,
            fullName: '',
            character: '',
            setstring:'',
            rename:'',
            idofble: '',
            isButtonVisible: false,
            blename: ''
        }
    }
    componentDidMount() {

        Blemanager.start({ showAlert: false });
        this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);

        NativeAppEventEmitter
            .addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral);

        Blemanager.scan([], 5, true)
            .then((results) => { console.log('Scanning...'); console.log(results)});
        if (Platform.OS === 'android' && Platform.Version >= 23) {
            PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
                if (result) {
                    console.log("Permission is OK");
                } else {
                    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
                        if (result) {
                            console.log("User accept");
                        } else {
                            console.log("User refuse");
                        }
                    });
                }
            });
        }
        this.animationn.play();

        AsyncStorage.getItem("blename").then((value) => {
            this.setState({ blename: value });
            console.log("ssss", this.state.blename)

        }).done(); AsyncStorage.getItem("character").then((value) => {
            this.setState({ character: value });
            console.log("ssss", this.state.character)

        }).done();
        AsyncStorage.getItem("setstring").then((value) => {
            this.setState({ setstring: value });
            console.log("ssss", this.state.setstring)

        }).done();
        AsyncStorage.getItem("rename").then((value) => {
            this.setState({ rename: value });
            console.log("ssss", this.state.rename)

        }).done();
    }


    dataget(target, value) {
        AsyncStorage.setItem(target, value);
        this.setState({ [target]: value });
    }

    animate() {
        setTimeout(() => {
            this.props.navigation.navigate('Sucess');
            this.setState({ isButtonVisible: false });
        }, 3000);
    }

    handleDiscoverPeripheral(data) {
        const { ble } = this.state
        this.setState({ ble: [...ble, data] }, () => {
         //    console.log('Got ble data ', ble);
            //console.log('local name');
            //console.log(data.advertising.localName);
        })
    }


    connectble() {
        if (this.state.blename != "") { console.log("ble is available");
            Blemanager.start({ showAlert: false });
            this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);

            NativeAppEventEmitter
                .addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral);

            Blemanager.scan([], 5, true)
                .then((results) => { console.log('Scanning...'); console.log(results);});

                

            if (this.state.ble.find(obj => obj.advertising.localName === this.state.blename)) {


                const bluetoothInstance = this.state.ble.find(obj => obj.advertising.localName === this.state.blename)
                this.setState({ idofble: bluetoothInstance.id })



                Blemanager.connect(bluetoothInstance.id)
                    .then(() => {

                        console.log('Connected!!');
                        console.log(this.state.idofble);
                        ToastAndroid.show('Connected!!', ToastAndroid.SHORT);
                        Blemanager.retrieveServices(this.state.idofble)
                            .then((peripheralInfo) => {

                                this.sendchar();

                                console.log('Peripheral info:', peripheralInfo);
                            });

                    })
                    .catch((error) => {

                        console.log("error:", error);
                    });

            }
            else {
                this.toast1();
            }
        }

        else {

            this.toast();
        }
    }




    setstringble() {
        if (this.state.blename != "") {
            Blemanager.start({ showAlert: false });
            this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);

            NativeAppEventEmitter
                .addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral);

            Blemanager.scan([], 5, true)
                .then((results) => { console.log('Scanning...'); });

                

            if (this.state.ble.find(obj => obj.advertising.localName === this.state.blename)) {


                const bluetoothInstance = this.state.ble.find(obj => obj.advertising.localName === this.state.blename)
                this.setState({ idofble: bluetoothInstance.id })



                Blemanager.connect(bluetoothInstance.id)
                    .then(() => {

                        console.log('Connected!!');
                        console.log(this.state.idofble);
                        ToastAndroid.show('Connected!!', ToastAndroid.SHORT);
                        Blemanager.retrieveServices(this.state.idofble)
                            .then((peripheralInfo) => {

                                this.setstr();

                                console.log('Peripheral info:', peripheralInfo);
                            });

                    })
                    .catch((error) => {

                        console.log("error:", error);
                    });

            }
            else {
                this.toast1();
            }
        }

        else {

            this.toast();
        }
    }



    
    setrenameble() {
        if (this.state.blename != "") {
            Blemanager.start({ showAlert: false });
            this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);

            NativeAppEventEmitter
                .addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral);

            Blemanager.scan([], 5, true)
                .then((results) => { console.log('Scanning...'); });

                

            if (this.state.ble.find(obj => obj.advertising.localName === this.state.blename)) {


                const bluetoothInstance = this.state.ble.find(obj => obj.advertising.localName === this.state.blename)
                this.setState({ idofble: bluetoothInstance.id })



                Blemanager.connect(bluetoothInstance.id)
                    .then(() => {

                        console.log('Connected!!');
                        console.log(this.state.idofble);
                        ToastAndroid.show('Connected!!', ToastAndroid.SHORT);
                        Blemanager.retrieveServices(this.state.idofble)
                            .then((peripheralInfo) => {

                                this.setrename();

                                console.log('Peripheral info:', peripheralInfo);
                            });

                    })
                    .catch((error) => {

                        console.log("error:", error);
                    });

            }
            else {
                this.toast1();
            }
        }

        else {

            this.toast();
        }
    }

    toast() {
        ToastAndroid.show('Enter your Bluetooth name', ToastAndroid.SHORT);
    }

    toast1() {
        ToastAndroid.show('No bluetooth found !', ToastAndroid.SHORT);
    }

    diconnect() {
        Blemanager.disconnect(this.state.idofble)
            .then(() => {
                console.log('Disconnected');
                ToastAndroid.show('Disconnected!!', ToastAndroid.SHORT);

            })
            .catch((error) => {
                console.log(error);
            });
    }

    sendchar() {

        sendgift = gift + this.state.character
          sendd = stringToBytes(sendgift)
        console.log('>>>>>>>>>>before write');
        Blemanager.writeWithoutResponse(this.state.idofble, 'FFE0', 'FFE1', sendd)

            .then(() => {
                console.log('.....Write: ' + sendd);
                ToastAndroid.show('Data Sent!!', ToastAndroid.SHORT);

                this.setState({ isButtonVisible: true });
                this.animation.play();
                this.diconnect();
                this.animate();
            })
            .catch((error) => {
                console.log(error);
            });
    }


    
    setstr() {

        settingstr = str + this.state.setstring
          senddd = stringToBytes(settingstr)
        console.log('>>>>>>>>>>before write');
        Blemanager.writeWithoutResponse(this.state.idofble, 'FFE0', 'FFE1', senddd)

            .then(() => {
                console.log('.....Write: ' + senddd);
                ToastAndroid.show('String set!!', ToastAndroid.SHORT);

             
                this.diconnect();
               
            })
            .catch((error) => {
                console.log(error);
            });
    }


    setrename() {

        settingstr = renameble + this.state.rename
          senddd = stringToBytes(settingstr)
        console.log('>>>>>>>>>>before write');
        Blemanager.writeWithoutResponse(this.state.idofble, 'FFE0', 'FFE1', senddd)

            .then(() => {
                console.log('.....Write: ' + senddd);
                ToastAndroid.show('Ble name updated!!', ToastAndroid.SHORT);

             
                this.diconnect();
               
            })
            .catch((error) => {
                console.log(error);
            });
    }

    render() {
        const { isButtonVisible } = this.state;

        return (
            <Container>

                <StatusBar barStyle="dark-content" hidden={true} backgroundColor="#00BCD4" translucent={true} />

                <Header style={{ backgroundColor: '#141e26' }}>

                    <Body>
                        <Image style={{ width: 150, height: 50, resizeMode: 'center', alignSelf: 'center' }} source={require('../images/Ebot-logo.png')} />
                    </Body>

                </Header>

                <View style={styles.container}>
                    <LottieView
                        ref={animation => {
                            this.animationn = animation;
                        }}
                        source={require('../animation/2843-mobile-app.json')}
                        style={{ width: '50%', backgroundColor: '#22303a' }}
                    />

                    {isButtonVisible &&
                        <LottieView
                            ref={animation => {
                                this.animation = animation;
                            }}
                            source={require('../animation/newAnimation.json')}
                            style={{ height: undefined, flex: 1, zIndex: 1, marginTop: -38, backgroundColor: '#22303a' }}
                        />
                    }

                    <View style={styles.inputContainerr}>

                        <Image style={styles.inputIcon} source={{ uri: 'https://png.icons8.com/bluetooth/50/3498db' }} />
                        <TextInput style={styles.inputs}
                            placeholder="Bluetooth Name"
                            keyboardType="email-address"
                            underlineColorAndroid='transparent'
                            // onChangeText={(fullName) => this.setState({ fullName })}
                            //onChangeText={(fullName) => this.dataget( fullName )}
                            onChangeText={(text) => this.dataget("blename", text)}
                            value={this.state.blename}

                        />
                    </View>


    
                    <TouchableOpacity style={{alignSelf:'flex-start',alignSelf:'center',marginTop:20}}>
                            <Text style={{ color: 'white', fontSize: 13, fontWeight: 'normal',  fontWeight: 'bold',position:'relative',padding:10,borderRadius:60, }}>Send character</Text>
                        </TouchableOpacity>   


                        <View style={{ flexDirection : 'row'}}>
                    <View style={styles.inputContainer}>
                        <Image style={styles.inputIcon} source={{ uri: 'https://png.icons8.com/keyboard/50/3498db' }} />
                        <TextInput style={styles.inputs}
                            placeholder="Special Character"
                            keyboardType="email-address"
                            underlineColorAndroid='transparent'
                            value={this.state.character}
                            onChangeText={(text) => this.dataget("character", text)}
                        //onChangeText={(character) => this.dataget( character )}

                        //    onChangeText={(character) => this.setState({ character })}
                        />
                    </View>
                    <View>
                        
                        <TouchableOpacity onPress={() => this.connectble()}>
                                <Image style={{ width: 50, height: 50, resizeMode: 'center', alignSelf: 'center' }} source={require('../images/ebotlogo.png')} />
                            </TouchableOpacity>
                            
                        </View>
    </View>

                    <TouchableOpacity style={{alignSelf:'flex-start',alignSelf:'center',marginTop:10}}>
                            <Text style={{ color: 'white', fontSize: 13, fontWeight: 'normal',  fontWeight: 'bold',position:'relative',padding:10,borderRadius:60, }}>Bluetooth rename</Text>
                        </TouchableOpacity>   
              
                  <View style={{ flexDirection : 'row'}}>
                    <View style={styles.inputContainer}>
                        <Image style={styles.inputIcon} source={{ uri: 'https://png.icons8.com/rename/50/3498db' }} />
                        <TextInput style={styles.inputs}
                            placeholder="Special Character"
                            keyboardType="email-address"
                            underlineColorAndroid='transparent'
                            value={this.state.rename}
                            onChangeText={(text) => this.dataget("rename", text)}
                        //onChangeText={(character) => this.dataget( character )}

                        //    onChangeText={(character) => this.setState({ character })}
                        />
                    </View>
                    <View>
                        
                        <TouchableOpacity onPress={() => this.setrenameble()}>
                                <Image style={{ width: 50, height: 50, resizeMode: 'center', alignSelf: 'center' }} source={require('../images/ebotlogo.png')} />
                            </TouchableOpacity>
                            
                        </View>
    </View>

                    <TouchableOpacity style={{alignSelf:'flex-start',alignSelf:'center',marginTop:10}}>
                            <Text style={{ color: 'white', fontSize: 13, fontWeight: 'normal',  fontWeight: 'bold',position:'relative',padding:10,borderRadius:60, }}>Set control string </Text>
                        </TouchableOpacity>   
              
                        <View style={{ flexDirection : 'row'}}>
                    <View style={styles.inputContainer}>
                        <Image style={styles.inputIcon} source={{ uri: 'https://png.icons8.com/numbers/50/3498db' }} />
                        <TextInput style={styles.inputs}
                            placeholder="Special Character"
                            keyboardType="email-address"
                            underlineColorAndroid='transparent'
                            value={this.state.setstring}
                            onChangeText={(text) => this.dataget("setstring", text)}
                        //onChangeText={(character) => this.dataget( character )}

                        //    onChangeText={(character) => this.setState({ character })}
                        />
                    </View>
                    <View>
                        
                        <TouchableOpacity onPress={() => this.setstringble()}>
                                <Image style={{ width: 50, height: 50, resizeMode: 'center', alignSelf: 'center' }} source={require('../images/ebotlogo.png')} />
                            </TouchableOpacity>
                            
                        </View>
    </View>

                    {/* <View>
                        <TouchableOpacity onPress={() => this.connectble()}>
                            <Image style={{ width: 100, height: 100, resizeMode: 'center', alignSelf: 'center' }} source={require('../images/ebotlogo.png')} />
                            <Text style={{ color: 'white', fontSize: 13, fontWeight: 'normal', marginTop: -4, alignSelf: 'center', color: 'white', fontWeight: 'bold' }}>E-bot Gift</Text>
                        </TouchableOpacity>
                    </View> */}

                    
              
                   
             
            </View>

           
                   
            </Container>


        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#22303a',

    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#FFFFFF',
        borderRadius: 30,
        borderBottomWidth: 1,
        width: 230,
        height: 45,
      alignSelf:'flex-start',
        flexDirection: 'row',
        alignItems: 'center',

    },
    inputContainerr: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#FFFFFF',
        borderRadius: 30,
        borderBottomWidth: 1,
        width: 230,
        height: 45,
      alignSelf:'center',
        flexDirection: 'row',
        alignItems: 'center',

    },
    inputs: {
        height: 45,
        marginLeft: 16,
        borderBottomColor: '#FFFFFF',
        flex: 1,

    },
    inputIcon: {
        width: 30,
        height: 30,
        marginLeft: 15,
        justifyContent: 'center'
    },
    buttonContainer: {
        height: 80,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: 80,
        borderRadius: 60,
    },
    signupButton: {
        backgroundColor: "#0263f4",
    },
    signUpText: {
        color: 'white',
    }

});