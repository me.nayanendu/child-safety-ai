/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */


import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, ScrollView, Button, View, Alert, TextInput, Text, Image, ImageBackground} from 'react-native';

const styles = StyleSheet.create({
  fulpg:{width:'100%', flex:1, flexDirection:'column', justifyContent: 'space-between', height:'100%',},
  whtbg:{backgroundColor:'#ffffff',},
  picbox2: {width:'100%', flex:1, flexDirection:'column',paddingTop:20, paddingBottom:20,},
  picbox: {alignSelf: 'center', width:254, height:254, borderColor:'#d8d8d8', borderStyle:'solid', borderWidth:2,},
  inpset:{ marginBottom:15, },
  label:{ color:'#000', fontSize:14, marginBottom:10,  },
  input1:{ backgroundColor:'#ffffff', borderColor:'#c2c2c2', borderStyle:'solid', borderWidth:1, height:42, borderRadius:2,
    paddingLeft:12, paddingRight:12, color:'#000000', },
  frmBx:{ paddingLeft:20, paddingRight:20, paddingBottom:20,  },
  tnBx5:{ paddingTop:10,},
  btnX1:{ backgroundColor:'#4a3ad0', paddingTop:11, paddingBottom:13, borderRadius:2,  },
  btnX1t:{ textAlign:'center', color:'#ffffff', },
  btnPnl:{flex:1, flexDirection:'row', paddingLeft:20, paddingBottom:20, paddingRight:20,},
  btn2:{paddingRight:7, width:'50%',},
  btnX2:{backgroundColor:'#c6cbd0', paddingTop:11, paddingBottom:13, borderRadius:2,},
  btnX2t:{textAlign:'center', color:'#202325',},
  btn3:{paddingLeft:7, width:'50%',},
  btnX3:{backgroundColor:'#4a3ad0', paddingTop:11, paddingBottom:13, borderRadius:2,},
  btnX3t:{textAlign:'center', color:'#ffffff',},


  lstbg:{backgroundColor:'#EEEEF3',paddingLeft:20, paddingRight:20,paddingTop:20,paddingBottom:5,},
  lstBx:{marginBottom:15,},
  lstClr:{backgroundColor:'#fff',borderRadius:2,},

  lSet1:{padding:10, borderBottomWidth:1, borderStyle:'solid', borderColor:'#E4E4E4',},
  ltxt1:{color:'#000', fontSize:14,},

  lSet2:{padding:10, flex:1, flexDirection:"row", },

  lpic:{width:60, height:60, borderRadius:16, backgroundColor:'#EEEEF3', },
  lname:{flex:1, paddingLeft:10, paddingRight:10,},
    ltxt3:{fontSize:14, color:'#000', fontWeight:'600',},
  lbtn:{ justifyContent:'center',},
    dltBtn:{backgroundColor:'#F55858',width:38, height:38,padding:10,borderRadius:19,}, 

});

export default class AnatomyExample extends Component {
render() {
return (



<View style={styles.fulpg}>
<ScrollView style={styles.whtbg}>

<View style={styles.picbox2}>
<View style={styles.picbox}>
  <Image style={{width:250, height:250}} source={require('./images/2.png')} />
</View>
</View>


<View style={styles.btnPnl}>
  <View style={styles.btn2}>
    <TouchableOpacity style={styles.btnX2} activeOpacity={0.8}>
      <Text style={styles.btnX2t}>Remove</Text>
    </TouchableOpacity>
  </View>
  <View style={styles.btn3}>
    <TouchableOpacity style={styles.btnX3} activeOpacity={0.8}>
      <Text style={styles.btnX3t}>Take Pic</Text>
    </TouchableOpacity>
  </View>
</View>


<View style={styles.frmBx}>
  
  <View style={styles.inpset}>
    <Text style={styles.label}>
      Hardware Name 
    </Text>
    <TextInput
        style={styles.input1}
        placeholder={'Hardware Name'}
        placeholderTextColor={'#9c9c9c'}
      />
  </View>

  <View style={styles.inpset}>
    <Text style={styles.label}>
      Charata
    </Text>
    <TextInput
        style={styles.input1}
        placeholder={'Charata'}
        placeholderTextColor={'#9c9c9c'}
      />
  </View>

  <View style={styles.tnBx5}>
    <TouchableOpacity style={styles.btnX1} activeOpacity={0.8}>
      <Text style={styles.btnX1t}>START</Text>
    </TouchableOpacity>
  </View>

</View>


<View style={styles.lstbg}>

  <View style={styles.lstBx}>
  <View style={styles.lstClr}>

    <View style={styles.lSet1}> 
      <Text style={styles.ltxt1}>No. 001</Text>
    </View>

    <View style={styles.lSet2}> 
      
        <View style={styles.lpic}>
          <Image style={{width:60, height:60, borderRadius:16,}} source={require('./images/5.png')} />
        </View>

        <View style={styles.lname}>
          <Text style={styles.ltxt3}>Anaisha Roy Anaisha Roy Anaisha Roy</Text>
        </View>
       
      <View style={styles.lbtn}>
        <TouchableOpacity style={styles.dltBtn} activeOpacity={0.8}>
          <Image style={{width:18, height:18}} source={require('./images/bin.png')} />
        </TouchableOpacity>
      </View>
    </View>

  </View>
  </View>

  <View style={styles.lstBx}>
  <View style={styles.lstClr}>

    <View style={styles.lSet1}> 
      <Text style={styles.ltxt1}>No. 002</Text>
    </View>

    <View style={styles.lSet2}> 
      
        <View style={styles.lpic}>
          <Image style={{width:60, height:60, borderRadius:16,}} source={require('./images/2.png')} />
        </View>

        <View style={styles.lname}>
          <Text style={styles.ltxt3}>Anaisha Roy Anaisha Roy Anaisha Roy</Text>
        </View>
       
      <View style={styles.lbtn}>
        <TouchableOpacity style={styles.dltBtn} activeOpacity={0.8}>
          <Image style={{width:18, height:18}} source={require('./images/bin.png')} />
        </TouchableOpacity>
      </View>
    </View>

  </View>
  </View>

  <View style={styles.lstBx}>
  <View style={styles.lstClr}>

    <View style={styles.lSet1}> 
      <Text style={styles.ltxt1}>No. 003</Text>
    </View>

    <View style={styles.lSet2}> 
      
        <View style={styles.lpic}>
          <Image style={{width:60, height:60, borderRadius:16,}} source={require('./images/4.png')} />
        </View>

        <View style={styles.lname}>
          <Text style={styles.ltxt3}>Anaisha Roy Anaisha Roy Anaisha Roy</Text>
        </View>
       
      <View style={styles.lbtn}>
        <TouchableOpacity style={styles.dltBtn} activeOpacity={0.8}>
          <Image style={{width:18, height:18}} source={require('./images/bin.png')} />
        </TouchableOpacity>
      </View>
    </View>

  </View>
  </View>











</View>


</ScrollView>
</View>     
);
}
}
